module.exports = function(params,data){
    if(!data){
        console.error('Bad data',data);
        return;
    }

    if(data.repository == undefined || typeof data.repository.name != 'string'){
        console.error('Repository is not defined',data.repository);
        return;
    }

    params.repository = data.repository.name;

    if(data.push == undefined ||  typeof data.push.changes != 'object' || data.push.changes.length == undefined){
        console.warn('Push is empty, repo:' + params.repository,data.push,data);
        return;
    }
    if(hasChangesInBranch(params,data.push.changes)){
        params.done = true;
    }
    return params;
};


  function hasChangesInBranch(params,changes){
    var hasChanges = false;
    for(var i = 0; i < changes.length; i++){
        //console.log(changes[i].new,changes[i].new.name == params.branch, changes[i].new.name, params.branch)
        if(changes[i].new == undefined){
             console.warn('New changes is empty in repo:' + params.repository, changes[i].new)
        }else{
            if(changes[i].new.type == undefined){
                console.warn('Undefined type of push in repo:' + params.repository, changes[i].new.type,changes[i].new)
            }
            if(changes[i].new.type == 'branch' && changes[i].new.name == params.branch){
                hasChanges = true;
                break;
            }
        }
    }
    return hasChanges;
}
