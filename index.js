var express = require('express'),
    http = require('http'),
    bodyParser = require('body-parser'),
    config = require('./config'),
    parseData = require('./parseData');

    app = express();

app.set('port', process.env.PORT || 8888);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.get('/', function (req, res, next) {
    res.send('auto deploy server is working')
});

app.post('/deploy/', function (req, res) {
    var spawn = require('child_process').spawn,
    params = parseData.fromBitBucket({branch:config.triggerOnBranch},req.body), deploy;
    console.log(params);
    if(params && params.done){
        deploy = spawn('sh', ['./deploy.sh', params.repository, params.branch]);
        console.log(params.repository);
        deploy.stdout.on('data', function (data) {
            console.log(''+ data);
        });
        deploy.on('close', function (code) {
            console.warn(params.repository,'Child process exited with code ' + code);
        });
    }
    res.status(200).json({message: 'Hook received!'})
});

http.createServer(app).listen(app.get('port'), function(){
    console.log('Express server listening on port ' + app.get('port'));
});